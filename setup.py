from setuptools import find_packages, setup

# README
with open('README.md', 'r', encoding='utf-8') as fh:
    long_description = fh.read()

def get_data_files():
    data_files = [
        ('share/doc/difftime', ['README.md', 'LICENSE']),
    ]
    return data_files

def get_repo():
    return 'https://gitlab.com/script-anon/eoponysaver'

setup(
    name='eoponysaver',
    version='0.1.0',
    license='MIT',
    author='Script Filly',
    author_email='anonymous.indefinitely@gmail.com',
    description='Save your outpost pony\'s properties and exact colors',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url=get_repo(),
    data_files=get_data_files(),
    project_urls={
        'Bug Tracker': f'{get_repo()}/issues',
    },
    classifiers=[
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent'
    ],
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    python_requires='>=3.10',
    install_requires=[],
    entry_points={
        'console_scripts': [
            'eoponysaver = eoponysaver.eoponysaver:main',
        ],
    },
    test_suite='tests',
)
