# eoponysaver

An archiving and artist's tool for Everfree Outpost.

Save your outpost pony's properties and obtain exact hex colors.

## Install

To install from the repository:

```bash
pip install -e .
```

To install from `pypi`:

```bash
pip install eoponysaver
```

## Usage

```bash
eoponysave "My Cute OC" "F U B A 1 4 5 5 4 2 7"
```

The input string expects the following inputs separated by whitespace:

1. Gender: [F]emale or [M]ale (either mare or stallion)
2. Tribe: [E]arth, [P]egasus, [U]nicorn
3. Mane Type: A B C (Long, Curly, Sharp)
4. Tail Type: A B C (Long, Curly, Sharp)
5. Eye Type: 1 2 3 4 (Blue, Green, Purple, Violet)

6-8. Coat R G B: (1-7)

8-11. Mane R G B: (1-7)

Your pony will be saved in the data directory of your os platform
with the given name as a json file.

### Pipenv

If you just want to test out `eoponysaver` and
you have `pipenv` installed:

```bash
pipenv run eoponysaver
```
