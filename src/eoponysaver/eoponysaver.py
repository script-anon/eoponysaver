# Standard Library
import os
import sys
import json
from platformdirs import PlatformDirs

# eoponsave: Saves an eopony's properties to disk, alongside the exact colors of the horse
# Usage: eoponysave "My Cute OC" "F U B A 1 4 5 5 4 2 7"

dirs = PlatformDirs("eoponysaver", "Script Filly")

# Global definitions
GENDERS = {
    "F": "Mare",
    "M": "Stallion",
}

TRIBES = {
    "E": "Earth",
    "U": "Unicorn",
    "P": "Pegasus",
}

MANES = {
    "A": "Long",  # Fluttershy Mane
    "B": "Curly", # Carrot Top Mane
    "C": "Sharp", # Rainbow Dash Mane
}

TAILS = {
    "A": "Long",  # Fluttershy Tail
    "B": "Curly", # Carrot Top Tail
    "C": "Sharp", # Rainbow Dash Tail
}

EYES = {
    1: ("Blue"  , "#9CDCF4"), # Pinkie Pie
    2: ("Green" , "#78D863"), # Applejack
    3: ("Purple", "#8D5DA4"), # Twilight Sparkle
    4: ("Violet", "#D9539D"), # Rainbow Dash
} # None available for Rarity or Fluttershy

COLOR_TABLE = [ 0x00, 0x22, 0x44, 0x66, 0x88, 0xaa, 0xcc, 0xff ]

# Parsing inputs
(_, name, line) = sys.argv

pony_array = line.split(' ')

(gender, tribe, mane, tail, eyes,
coat_r, coat_g, coat_b,
mane_r, mane_g, mane_b) = pony_array

# We want to create some data structures with this information so we can serialize to multiple formats
eyes_pair = EYES[int(eyes)]

# Convert the human readable input into the format eo-cli-chat expects
def get_sex(gender):
    return 0 if gender == 'F' else 1

def get_tribe(tribe):
    match tribe:
        case "E": return 0
        case "P": return 1
        case "U": return 2

def get_mane_tail_index(x):
    match x:
        case "A": return 0
        case "B": return 1
        case "C": return 2

def get_appearance(gender, tribe, mane, tail, eyes,
                   coat_r, coat_g, coat_b,
                   mane_r, mane_g, mane_b,):

    zindex = lambda x: (int(x)) - 1
    appearance = [
        get_sex(gender), get_tribe(tribe), get_mane_tail_index(mane),
        get_mane_tail_index(tail), zindex(eyes),
        zindex(coat_r), zindex(coat_g), zindex(coat_b),
        zindex(mane_r), zindex(mane_g), zindex(mane_b),
        0, 0 ,0
    ]
    return appearance

# Calculate the colors exactly from the pony
# TODO: Find exact outline calculation shit

def get_eye_color(eyes):
    eyes_pair = EYES[int(eyes)]
    return eyes_pair[1]

def from_8bit_to_rgb(r, g, b):
    return COLOR_TABLE[r], COLOR_TABLE[g], COLOR_TABLE[b]

def rgb_to_hex(r, g, b):
    return ('{:X}{:X}{:X}').format(r, g, b)

def get_fill(r, g, b):
    _8bit = from_8bit_to_rgb(r,g,b)
    return rgb_to_hex(*_8bit)

def get_outline(r, g, b):
    # TODO: Darken values
    # b46090
    # (255 – 180) = 75, (136 – 96) = 40, (204 - 144) = 60
    _8bit = from_8bit_to_rgb(r,g,b)
    (r, g, b) = _8bit
    (r, g, b) = r * 2, g * 2, b * 2
    # return rgb_to_hex(*_8bit)
    return rgb_to_hex(r,g,b)

def get_colors(eyes,
                    coat_r, coat_g, coat_b,
                    mane_r, mane_g, mane_b):

    to_ints = lambda xs: map (int, xs)
    (coat_r, coat_g, coat_b) = to_ints([coat_r, coat_g, coat_b])
    (mane_r, mane_g, mane_b) = to_ints([mane_r, mane_g, mane_b])

    pony_colors = {
        'Eyes': get_eye_color(eyes),
        'Coat': {
            'Fill': get_fill(coat_r, coat_g, coat_b),
            'Outline': get_outline(coat_r, coat_g, coat_b),
        },
        'Mane': {
            'Fill': get_fill(mane_r, mane_g, mane_b),
            'Outline': get_outline(mane_r, mane_g, mane_b),
        },
    }
    return pony_colors

def insert_every(n, s, sep=' '):
    return sep.join(s[i:i+n] for i in range(0, len(s), n))

def fmt_pretty_appearance(appearance):
    s1 = appearance[0:3]
    s2 = appearance[4:]
    return f'{s1}, {insert_every(6, s2)}'

def get_pony_dir() -> str:
    return f'{dirs.user_data_dir}/ponies'

def fmt_pony_file_path(fname) -> str:
    return f'{get_pony_dir()}/{fname}'

# Serialize
def main():
    appearance = get_appearance(gender, tribe, mane, tail, eyes,
                               coat_r, coat_g, coat_b,
                               mane_r, mane_g, mane_b,)
    appearance = ','.join(str(x) for x in appearance)

    colors = get_colors(eyes,
                        coat_r, coat_g, coat_b,
                        mane_r, mane_g, mane_b)

    pony = {
        'Name': name,
        'Gender': GENDERS[gender],
        'Tribe': TRIBES[tribe],
        'Mane': MANES[mane],
        'Tail': TAILS[tail],
        'Eyes': eyes_pair[0],
        'Input': line,
        # 'Appearance': appearance,
        'Appearance': fmt_pretty_appearance(appearance),
        'Colors': colors,
    }

    if not os.path.exists(get_pony_dir()):
        os.makedirs(get_pony_dir())

    # Write to disk
    ponyfp = fmt_pony_file_path(f'{name}.json')
    with open(ponyfp, 'w') as fp:
        json.dump(pony, fp, indent=4)

    print(ponyfp)
